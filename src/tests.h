//
// Created by viacheslav on 11.12.22.
//


#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"

#include <stdlib.h>


#define DEBUG_FILE stderr
#define OUTPUT_FILE stdout
#define SAMPLE (MID_SAMPLE * 4)
#define MID_SAMPLE (MINI_SAMPLE * 2)
#define MINI_SAMPLE REGION_MIN_SIZE

typedef bool (*test)();

extern void run_tests();


#endif //MEMORY_ALLOCATOR_TESTS_H
