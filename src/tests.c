//
// Created by viacheslav on 11.12.22.
//

#define _DEFAULT_SOURCE

#include "tests.h"
#include "sys/mman.h"

static void _munmap(void *heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity)
                                            {.bytes = size}).bytes);
}


/* Test malloc works */
bool test_malloc() {
    void *heap = heap_init(SAMPLE);
    debug_heap(DEBUG_FILE, heap);

    void *mem = _malloc(MID_SAMPLE);
    debug_heap(DEBUG_FILE, heap);

    if (!(heap && mem)) {
        return false;
    }

    _free(mem);
    debug_heap(DEBUG_FILE, heap);

    _munmap(heap, SAMPLE);

    return true;

}

/* Free multiple allocated blocks */
bool test_free_multiple() {
    void *heap = heap_init(SAMPLE);

    /* allocate 4 blocks */
    void *mem1 = _malloc(MINI_SAMPLE);
    debug_heap(DEBUG_FILE, heap);
    void *mem2 = _malloc(MINI_SAMPLE);
    debug_heap(DEBUG_FILE, heap);
    void *mem3 = _malloc(MINI_SAMPLE);
    debug_heap(DEBUG_FILE, heap);
    void *mem4 = _malloc(MINI_SAMPLE);
    fprintf(DEBUG_FILE, "heap after malloc:\n");
    debug_heap(DEBUG_FILE, heap);

    if (!(heap && mem1 && mem2 && mem3 && mem4)) {
        return false;
    }

    _free(mem1);
    //debug_heap(DEBUG_FILE, heap);
    _free(mem3);

    fprintf(DEBUG_FILE, "heap after 2 frees:\n");
    debug_heap(DEBUG_FILE, heap);

    _munmap(heap, SAMPLE);

    return true;
}

/* Check if extends */
bool test_malloc_extend() {
    void *heap = heap_init(MID_SAMPLE);
    debug_heap(DEBUG_FILE, heap);

    fprintf(DEBUG_FILE, "Allocation in process...\n");
    void *block = _malloc(SAMPLE);
    debug_heap(DEBUG_FILE, heap);

    _free(block);
    _munmap(heap, MID_SAMPLE);

    return true;


}


bool test_malloc_no_extend() {
    void *heap = heap_init(MINI_SAMPLE);
    struct block_header *head = (struct block_header *) heap;

    debug_heap(DEBUG_FILE, heap);

    fprintf(DEBUG_FILE, "Allocation in process...\n");

    void *huge_block = _malloc(head->capacity.bytes);
    debug_heap(DEBUG_FILE, heap);

    struct block_header *block_header = (struct block_header *) (huge_block - offsetof(struct block_header, contents));

    void *after_heap = (void *) block_header->contents + block_header->capacity.bytes;

    void *temp_map = mmap(after_heap,
                          MINI_SAMPLE,
                          PROT_READ | PROT_WRITE,
                          MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS,
                          -1,
                          0);

    fprintf(DEBUG_FILE, "Allocation of memory in new region...\n");

    void *next_block = _malloc(MINI_SAMPLE);
    debug_heap(DEBUG_FILE, heap);

    _free(huge_block);
    _free(next_block);
    _munmap(temp_map, MINI_SAMPLE);
    _munmap(heap, MINI_SAMPLE);

    return true;
}


test tests[] = {
        test_malloc,
        test_free_multiple,
        test_malloc_extend,
        test_malloc_no_extend
};


void run_tests() {
    size_t tests_passed = 0;
    fprintf(OUTPUT_FILE, "Tests running...\n");

    size_t num_tests = sizeof(tests) / sizeof(tests[0]);

    for (size_t i = 0; i < num_tests; ++i) {
        fprintf(OUTPUT_FILE, "---------Test %"PRId64"---------\n", i + 1);
        tests_passed += tests[i]();
        fprintf(OUTPUT_FILE, "--------------------------\n");
    }

    fprintf(OUTPUT_FILE, "%zu out of %zu tests passed", tests_passed, num_tests);
}
