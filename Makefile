CC=gcc
CFLAGS=--std=c17 -Wall -Wextra -Werror

BUILD=build
SRC=src

.PHONY: all clean test

all: $(BUILD)/mem.o $(BUILD)/util.o $(BUILD)/mem_debug.o $(BUILD)/tests.o $(BUILD)/main.o
	$(CC) -o $(BUILD)/main $^

build:
	mkdir -p $(BUILD)

$(BUILD)/%.o: $(SRC)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	$(RM) -rf $(BUILD)

test:
	@+cd tester; make CC=$(CC)